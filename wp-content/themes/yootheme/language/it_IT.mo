��    *      l  ;   �      �  
   �  	   �     �     �      �     �  
   �  	           $   3     X     `     n     �     �     �     �  W   �     �  a        n     s     x  	   �     �     �  %   �     �     �     �     �  F        N  #   U  \   y  
   �     �     �     �       $     Z  A  
   �     �     �     �     �     �  
   �  
   �      	  $   %	     J	     S	     a	     q	     �	     �	     �	  Y   �	     �	  r   
     x
     }
     �
     �
     �
     �
     �
     �
     �
     �
       K        f  ,   l  f   �  	         
          (     7  !   F               *           $                               
   	       %                                "                   #      '                             )   (   &                  !                     % Comments %1$s Edit %1$s Previous %1$s at %2$s &#8592; Back to WordPress Editor ,  0 Comments 1 Comment <span uk-pagination-next></span> <span uk-pagination-previous></span> Comment Comments (%s) Comments are closed. Continue reading Edit Email Home It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Leave a Comment Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a> Menu Name Newer Comments Next %1$s Nothing Found Older Comments Oops! That page can&rsquo;t be found. Page Builder Pages: Posted in %1$s Posted in %1$s. Ready to publish your first post? <a href="%1$s">Get started here</a>. Search Search Results for &#8220;%s&#8221; Sorry, but nothing matched your search terms. Please try again with some different keywords. Tags: %1$s Website Written by %s on %s. Written by %s. Written on %s. Your comment is awaiting moderation. Project-Id-Version: VERSION
POT-Creation-Date: 2017-12-09 23:50+0000
PO-Revision-Date: 2017-12-09 23:50+0000
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE TEAM <EMAIL@ADDRESS>
Language: it_IT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 % commenti %1$s Modifica %1$s Precedente %1$s in %2$s &#8592; Torna all'editor ,  0 commenti 1 commento <span uk-pagination-next></span> <span uk-pagination-previous></span> Commento Commenti (%s) Commenti Chiusi Continua a leggere Modifica Email Home Sembra che non riusciamo a trovare cosa cerchi. Probabilmente la ricerca ti può aiutare. Lascia un commento Collegato come <a href="%1$s">%2$s</a>. <a href="%3$s" title="Scollegati dall&rsquo;account">Vuoi scollegarti?</a> Menu Nome Nuovi Commenti Successivo %1$s Nessun risultato Vecchi Commenti Oops! La pagina non esiste. Page Builder Pagine: Pubblicato in %1$s Pubblicato in %1$s. Sei pronto per pubblicare il tuo primo post? <a href="%1$s">Inizia qui</a>. Cerca Risultati della ricerca per &#8220;%s&#8221; Spiacente, ma nessun risultato corrisponde alla tua ricerca. Prova di nuovo con parole chiave diverse. Tag: %1$s Sito web Scritto da %s il %s. Scritto da %s. Scritto il %s. Commento in attesa di moderazione 